package ru.minestile.VKBot;

/**
 *
 * @author mineStile
 */
public class RunnableThread extends Thread {
   private Thread t;
   private final String threadName;
   
   RunnableThread( String name) {
      threadName = name;
   }
   
   @Override
   public void run() {
      
   }
   
   @Override
   public void start () {
      if (t == null) {
         t = new Thread(this, threadName);
         t.start();
      }
   }
}
