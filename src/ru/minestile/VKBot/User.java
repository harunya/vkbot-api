/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.minestile.VKBot;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class User {
    private String[] permissions;
    private final VKBot vkBot;
    
    public UserInfo userInfo;
    public String usersFileName = "users.json";
    public String groupsFileName = "groups.json";
    public Boolean enablePrint = true;
    
    
    User(VKBot vkBot, UserInfo ui){
        this.userInfo = ui;
        this.vkBot = vkBot;
        
        this.update();
    }
    
    private String loadFile(String url){
        try {
            List<String> file = Files.readAllLines(Paths.get(url));
            String content = "";
            for(int i=0;i<file.size();i++)
                content += file.get(i) + "\n";
            
            return content;
            
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    private void saveFile(String filename, String contents){
        try {
            Files.write( Paths.get(filename), contents.getBytes(), StandardOpenOption.CREATE);
            //(new BufferedWriter( new FileWriter(filename))).write(contents);
        } catch (Exception ex) {
            vkBot.sendMessage(userInfo.peerId, "Failed write to file \"" +
                    filename + "\" conentents \"" + contents + "\": "+ex.getMessage());
        }
    }
    
    private Object parseJSON(String text){
        Object obj = null;
        try {
            JSONParser parser = new JSONParser();
            obj = parser.parse(text);
        } catch (ParseException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
    
    private void update(){
        JSONObject groups = (JSONObject) parseJSON(loadFile(groupsFileName));
        JSONObject users = (JSONObject) parseJSON(loadFile(usersFileName));
        
        
        if(users.containsKey(this.userInfo.userId+"")){
            String group = (String) users.get(this.userInfo.userId+"");
            
            if(!groups.containsKey(group)){
                this.permissions = new String[0];
                VKBot.logE("Group "+group+" for user "+userInfo.userName+" not exists!");
                return;
            }
                
            JSONArray perms = (JSONArray) groups.get(group);
            
            this.permissions = new String[perms.size()];
            for(int i=0;i<perms.size();i++)
                this.permissions[i] = (String) perms.get(i);
            
            VKBot.logD(VKBot.implode(this.permissions, ", "));
        }else{
            this.vkBot.sendMessage(this.userInfo.peerId, "Virtual config allocated for you");
            
            users.put(userInfo.userId+"", "user");
            
            this.saveFile(usersFileName, users.toJSONString());
        }
        
    }
    public Boolean existsPermission(String perm){
        for (String permission : permissions) {
            if (permission.equals(perm)) {
                return true;
            }
        }
        
        if(enablePrint)
            this.vkBot.sendMessage(userInfo.peerId, userInfo.userName+", not access to this "
                + "command, need \"" + perm + "\" permission!");
        
        return false;
    }

    public String getPermissionsString() {
        String result = "Permissions [" + permissions.length + "]: ";
        for (String permission : permissions)
            result += permission;
        
        return result;
    }
}
