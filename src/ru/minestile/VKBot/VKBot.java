package ru.minestile.VKBot;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class VKBot {
	public Boolean is_auth = false;
	public Boolean debugMode = false;
        public Boolean threadsSupport = false;
        public Boolean permissionsSendErrors = true;
	
	private final String baseUrl = "https://api.vk.com/method/";
	private final String version = "5.92";
	private String access_token = "";
	private String group_id = "";
        private String usersFileName = null;
        private String groupsFileName = null;
	
	private final String[] lpData = {"", ""};
	private long ts = 0;
	
	private Map<String, VKBotCommandHandler> commandHandlers = new HashMap<String, VKBotCommandHandler>();
	private VKBotMessageHandler messageHandler = new VKBotMessageHandler();
	
	private VKBotReceiveHandler startReceiveHandler = new VKBotReceiveHandler();
	private VKBotReceiveHandler stopReceiveHandler = new VKBotReceiveHandler();
	
        
	public VKBot () {
		
	}
        
        public static int randInt(int min, int max){
            return (new Random()).nextInt((max - min) + 1) + min;
        }
	
	public static String implode(String[] strs, String glue) {
		String str = "";
		
		for(int i=0;i<strs.length;i++) {
			str += strs[i] + glue;
		}
		
		return str;
	}
	
	public static String implode(String[] strs, String glue, int rid) {
		String str = "";
		
		for(int i=0;i<strs.length;i++) {
			str += (i+rid) + strs[i] + glue;
		}
		
		return str;
	}
	
	public Boolean login (String access_token, String group_id) {
		String access_tokenD = this.access_token;
		
		this.access_token = access_token;
		
		if(getNickUser(163060509) != null) {
			this.is_auth = true;
			this.group_id = group_id;
			return true;
		}
		
		this.access_token = access_tokenD;
		this.is_auth = false;
		return false;
	}
        
        public void setPermissionsFilePaths(String users, String groups){
            this.usersFileName = users;
            this.groupsFileName = groups;
        }
        
        public static String replaceString(String pat, String str){
            Pattern pattern = Pattern.compile(pat);
            Matcher matcher = pattern.matcher(str);
            
            return matcher.replaceAll("");
        }
	
        public static String normalizeCommand(String cmd){
            cmd = replaceString("^\\[id[0-9]*|(.*)\\]", cmd);
            
            cmd = replaceString("^[ ]*", cmd);
            return cmd;
        }
        
	public void tick() {
		JSONArray LPData = connectToLP();
		
		if(debugMode)
			logD(LPData.toString());
		
		startReceiveHandler.onUse(this);
		
                
                
		for(int i=0;i<LPData.size();i++) {
                    final JSONObject obj = (JSONObject)LPData.get(i);
                    
                    if(threadsSupport)
                        new RunnableThread("thr-" + i){
                            public void run() {
                                execCommandBot(obj); 
                            }
                        }.start();
                    else
                        execCommandBot((JSONObject)LPData.get(i));
		}
		
		stopReceiveHandler.onUse(this);
	}
	
	private void execCommandBot(JSONObject LPData) {
		JSONObject respObject = ((JSONObject)LPData.get("object"));
		String text = (String) respObject.get("text");
                
                text = VKBot.normalizeCommand(text);
                
		String type = (String) LPData.get("type");
		long senderId = (long) respObject.get("from_id");
		long peerId = (long) respObject.get("peer_id");
		
		String userName = "";
		
		if(senderId > 0)
			userName = getNickUser(senderId);
		else
			userName = "[BOT?]";
		
		String[] cmd = text.split(" ");
		
		UserInfo ui = new UserInfo();
		ui.peerId = peerId;
		ui.userId = senderId;
		ui.userName = userName;
                
                User user = new User(this, ui);
                user.enablePrint = permissionsSendErrors;
                
                if(this.groupsFileName != null)
                    user.groupsFileName = this.groupsFileName;
                
                if(this.usersFileName != null)
                    user.usersFileName = this.usersFileName;
                
                
		messageHandler.onUse(this, user, text);
		
		if(commandHandlers.containsKey(cmd[0]))
			commandHandlers.get(cmd[0]).onUse(this, user, cmd);
		
	}
	
	public String getNickUser(long id) {
		return getNickUser(id, "nom");
	}
	
	public String getNickUser(long id, String namecase) {
		String info = sendGet(baseUrl+"users.get", "user_ids="+id+"&name_case="+namecase);
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(info);
			
			JSONObject array = (JSONObject)obj;
			
			String lastName = (String) ((JSONObject)((JSONArray) array.get("response")).get(0)).get("last_name");
			String firstName = (String) ((JSONObject)((JSONArray) array.get("response")).get(0)).get("first_name");
	        
	        return firstName + " " + lastName;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONArray getHistory(long peerId, long count) {
		String his = sendGet(baseUrl+"messages.getHistory", "peer_id="+peerId+"&count="+count);
		
		JSONParser parser = new JSONParser();
		try {
			System.out.println(his);
			Object obj = parser.parse(his);
			
			JSONObject array = (JSONObject)obj;
	        
	        return (JSONArray) ((JSONObject) array.get("response")).get("items");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public long getLatestMsgId(long peerId) {
		JSONArray his = getHistory(peerId, 1);
		
		
		return (long) ((JSONObject)his.get(0)).get("conversation_message_id");
	}
	
	public String sendMessage(long peerId, String text) {
		JSONParser parser = new JSONParser();
		try {
			String msgResp = sendGet(baseUrl+"messages.send", "peer_id="+peerId+"&message="+URLEncoder.encode(text, "UTF-8")+"&random_id=0");
			
			Object obj = parser.parse(msgResp);
			
                        String id = "0";
                        id = ((Long) ((JSONObject)obj).get("response"))+"";
                        
                        return id;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Err";
	}
	
	private JSONArray connectToLP() {
		String data = sendGet(lpData[1], "act=a_check&key="+lpData[0]+"&ts="+ts+"&wait=25");
		
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(data);
			
			JSONObject array = (JSONObject)obj;
	        
	        ts = Long.parseLong((String) array.get("ts"));
	        
	        return (JSONArray) array.get("updates");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void updateLPServer() {
		String resp = sendGet(baseUrl+"groups.getLongPollServer", "lp_version=3");
		JSONParser parser = new JSONParser();
		
		try {
			Object obj = parser.parse(resp);
			
			JSONObject array = (JSONObject)obj;
			
			if(debugMode)
				logD(array.toString());
	        
	        lpData[0] = (String) ((JSONObject)array.get("response")).get("key");
	        lpData[1] = (String) ((JSONObject)array.get("response")).get("server");
	        ts = Long.parseLong((String) ((JSONObject)array.get("response")).get("ts"));
	        
	        System.out.println("LPServer updated");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// HTTP GET request
	public String sendGet(String url, String query) {
		StringBuffer response = null;
		try {
			URL obj = new URL(url+"?"+query+"&v="+version+"&access_token="+access_token+"&group_id="+group_id);
			
			
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept-Charset", "UTF-8");
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(),"utf-8"));
			String inputLine;
			response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		//logD(response.toString());
		return response.toString();

	}
	
	public void addCommandHandler(String cmd, VKBotCommandHandler ch) {
		commandHandlers.put(cmd, ch);
	}
	
	public void addMessageHandler(VKBotMessageHandler ch) {
		this.messageHandler = ch;
	}
	
	public void addStartReceiveHandler(VKBotReceiveHandler ch) {
		this.startReceiveHandler = ch;
	}
	
	public void addStopReceiveHandler(VKBotReceiveHandler ch) {
		this.stopReceiveHandler = ch;
	}
	
	public void addReceiveHandler(VKBotReceiveHandler ch) {
		this.startReceiveHandler = ch;
		this.stopReceiveHandler = ch;
	}	
	
	public static void logE(String msg) {
		System.err.println("[ VKBot ]  "+msg);
	}
	
	public static void logD(String msg) {
		System.out.println("[ VKBot ]  "+msg);
	}
}
