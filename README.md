VKBotApi required a lib "JsonSimple": 
    https://code.google.com/archive/p/json-simple
or
    https://github.com/fangyidong/json-simple

Typic using this library:

```java
// Initialize main class
VKBot vkBot = new VKBot();

// Setup debugMode as true, enabling
// print in console ALL messages of work bot
vkBot.debugMode = true;

// Enabling thread support
vkBot.threadsSupport = true;

// Enabling send permissions errors to user (If need)
vkBot.permissionsSendErrors = true;
        
// setPermissionsFilePaths("Groups file", "Users file") is setup
// for permissions working
vkBot.setPermissionsFilePaths("groups.json", "users.json");
		
// For login using bots access_token and group_id (granted vk.com)
if(!vkBot.login("access_token", "groupId"))
	VKBot.logE("Error: failed login");

// Static method VKBot.logD(String message) print message as debug,
// VKBot.logE(String message) as error
VKBot.logD("Started VKBot");

// To interact with the user, the so-called "handlers" are used, 
// now available:

// ReceiveHandler
//     - startReceiveHandler
//     - stopReceiveHandler
// MessageHandler
// CommandHandler

// To install a handler, methods are used add[Handler_name](Arguments)
// For example:

// Warning! 2 argument VKBotMessageHandler and VKBotCommandHandler - UserInfo
// changed to User!

// Puts in console sended user messages
vkBot.addMessageHandler(new VKBotMessageHandler() {
	public Boolean onUse(VKBot vkBot, User user, String text) {
	    VKBot.logD(user.userInfo.userName + ": " + text);
	    return true;
	}
});

// Send message to user info by command "test"
vkBot.addCommandHandler("test", new VKBotCommandHandler() {
	public Boolean onUse(VKBot vkBot, User user, String[] args) {
		vkBot.sendMessage(user.userInfo.peerId, "Test! Hello, " +
		user.userInfo.userName + "! Your id: " + user.userInfo.userId +
		"\nWith args: " + VKBot.implode(args, ","));
		
		return true;
	}
});


// Updating LoopBackServer is required!
vkBot.updateLPServer();
		
// Main cycle, for accepting messages
while(true) {
	vkBot.tick();
}

```